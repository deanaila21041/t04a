import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            // edit here
            // Menjalankan perulangan sampai input valid diberikan
            try {
                System.out.print("Masukkan bilangan pembilang: ");
                int pembilang = scanner.nextInt();

                System.out.print("Masukkan bilangan penyebut: ");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasil pembagian: " + hasil);

                validInput = true;
            } catch (InputMismatchException e) {
                // Menangani kesalahan input yang bukan bilangan bulat/bilangan desimal
                System.out.println("Bilangan pembilang tidak boleh bukan bilangan bulat/bilangan desimal,  masukkan bilangan bulat");
                scanner.nextLine(); // Membersihkan input yang tidak valid dari scanner
            } catch (ArithmeticException e) {
                // Menangani kesalahan aritmatika, yaitu saat penyebut bernilai 0
                System.out.println(e.getMessage());
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        // add exception apabila penyebut bernilai 0
        if (penyebut == 0) {
            throw new ArithmeticException("Bilangan penyebut tidak boleh nol, masukkan bilangan selain nol");
        }

        return pembilang / penyebut;
    }
}
